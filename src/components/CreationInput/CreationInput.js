import React from 'react'
import classes from './CreationInput.module.css'

const CreationInput = props => {
    return(
        <input 
            className = {classes.CreationInput} 
            type = 'text' 
            placeholder = 'Enter New To Do' 
            onChange={(evt) => {
                if (!props.onChange) {
                    return;
                }
                
                props.onChange(evt.target.value)
            }}
            value = {props.value}
        >
        </input>
    )
} 

export default CreationInput