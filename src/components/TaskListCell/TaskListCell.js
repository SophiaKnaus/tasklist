import React from 'react'
import classes from './TaskListCell.module.css'

const widthCellMap = {
    name: classes.taskName,
    priority: classes.taskPriority,
    date: classes.taskDate,
    check: classes.taskCheck,
    delete: classes.taskDelete,
}



const TaskListCell = props => {
    const cls = [classes.TaskListCell,widthCellMap[props.type]]

    return(
        <div className = {cls.join(' ')}>
            {props.children}
        </div>
    )
}

export default TaskListCell