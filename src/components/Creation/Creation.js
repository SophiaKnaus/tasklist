import React from 'react'
import classes from './Creation.module.css'
import CreationInput from '../CreationInput/CreationInput'
import Butttons from '../Buttons/Buttons'


const Creation = props => {
    return(
        <div className = {classes.Creation}>
            <CreationInput 
                onChange={props.onChange}
                value={props.inputTaskName}
            />
            <Butttons onClick={props.onCreationOpen} />
        </div>
    )
}

export default Creation