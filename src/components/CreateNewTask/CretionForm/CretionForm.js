import React from 'react'
import classes from './CretionForm.module.css'
import Buttons from '../../Buttons/Buttons'
import TaskListRow from '../../TaskListRow/TaskListRow'
import TaskListCell from '../../TaskListCell/TaskListCell'
import CreationInput from '../../CreationInput/CreationInput'
import PriorityDote from '../../PriorutyDote/PriorutyDote'



class CretionForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            createTaskName: props.inputTaskName,
            createTaslPriority: 'Low Priority',
            createTaskDate: '',
        }
    }

    onChangeInput = (text) => {
        this.setState({
            createTaskName: text
        })
    }

    onChangeSelector = (evt) => {
        this.setState({
            createTaslPriority: evt.target.value
        })
    }

    onChangeDate = (evt) => {
        this.setState({
            createTaskDate: evt.target.value
        })
    }

    onCreateTask = () => {
        if (!this.state.createTaskName || !this.state.createTaskDate) {
            return;
        }
        
        this.props.addTask({ name: this.state.createTaskName, priority: this.state.createTaslPriority, date: this.state.createTaskDate, check: false, })
        this.props.onClose()
    }


    render() {
        return (
            <div className={classes.CretionForm}
                onClick={(evt) => {
                    evt.stopPropagation()
                }}
            >
                <p>CREATE NEW TASK</p>
                <div className={classes.Wrapper}>
                    <TaskListRow >
                        <TaskListCell>
                            <CreationInput
                                value={this.state.createTaskName}
                                onChange={this.onChangeInput}
                            >
                            </CreationInput>
                        </TaskListCell>
                        <TaskListCell>
                            <PriorityDote type={this.state.createTaslPriority} />
                            <select className={classes.SelectPriority} onChange={this.onChangeSelector}>
                                <option value='Low Priority' checked={this.state.createTaslPriority === "Low Priority"}>Low Priority</option>
                                <option value='Medium Priority' checked={this.state.createTaslPriority === 'Medium Priority'}>Medium Priority</option>
                                <option value='High Priority' checked={this.state.createTaslPriority === "High Priority"}>High Priority</option>
                            </select>
                        </TaskListCell>
                        <TaskListCell>
                            <input
                                className={classes.DateInput}
                                type='date'
                                value={this.state.createTaskDate}
                                onChange={this.onChangeDate}
                            >
                            </input>
                        </TaskListCell>
                    </TaskListRow></div>

                <div>
                    <Buttons icon='ok'
                        onClick={this.onCreateTask}
                    />
                    <Buttons icon='close' onClick={this.props.onClose} />
                </div>

            </div>
        )
    }
}

export default CretionForm