import React from 'react'
import classes from './CreateNewTask.module.css'
import CretionForm from './CretionForm/CretionForm'

const CreateNewTask = props => {
    if (!props.isOpen) {
        return null;
    }

    return(
        <div className = {classes.CreateNewTask} onClick={props.onClose}>
            <CretionForm
                onClose = {props.onClose}
                inputTaskName={props.inputTaskName}
                addTask={props.addTask}
            ></CretionForm>
        </div>
    )
}

export default CreateNewTask