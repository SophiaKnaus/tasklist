import React from 'react'
import classes from './TaskList.module.css'
import TaskListCell from '../TaskListCell/TaskListCell'
import TaskListRow from '../TaskListRow/TaskListRow'
import Buttons from '../Buttons/Buttons'
import PriorutyDote from '../PriorutyDote/PriorutyDote'


const TaskList = props => {
    return (
        <div className={classes.TaskList}>
            { props.tasks.map((task) => {
                return (
                    <TaskListRow
                        key={task.name}
                    >
                        <TaskListCell type='name'>
                            {task.name}
                        </TaskListCell>
                        <TaskListCell type='priority'>
                            <PriorutyDote type={task.priority} />{task.priority}
                        </TaskListCell>
                        <TaskListCell type='date'>
                            {task.date}
                        </TaskListCell>
                        <TaskListCell type='check' >
                            <input type='checkbox' className={classes.Checkbox}
                                onChange={() => props.onCheckBoxClick(task)}
                            ></input>
                        </TaskListCell>
                        <TaskListCell type='delete'>
                            <Buttons icon="delete" onClick={() => props.onDeleteTask(task)} />
                        </TaskListCell>
                    </TaskListRow>
                )
            })
            }
        </div>
    )
}

export default TaskList