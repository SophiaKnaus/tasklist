import React from 'react'
import classes from './DeleteAll.module.css'
import Buttons from '../../Buttons/Buttons'


const DeleteAll = props => {
    return(
        <div className = {classes.DeleteAll}>
            <p>Delete All</p><Buttons icon = 'delete' onClick = {props.onDeleteAll}/>
        </div>
    )
}

export default DeleteAll