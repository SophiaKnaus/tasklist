import React from 'react'
import classes from './TextArea.module.css';
import img from '../../../pictures/avatar.jpg';

const TextArea = props => {
    return(
        <div className = {classes.TextArea}>
            <p>TODO-LIST</p>
            <img src={img}/>
        </div>
    )
}

export default TextArea