import React from 'react'
import classes from './Content.module.css'
import TextArea from './TextArea/TextArea'
import Statistic from '../Statistic/Statistic'
import Creation from '../Creation/Creation'
import TaskList from '../TaskList/TaskList'
import DeleteAll from './DeleteAll/DeleteAll'


function getSuccessArrayLength(tasks) {
    return tasks.filter((task) => {
        return task.check
    }).length;
}

const Content = props => {
    return (
        <main className = {classes.Content}>
            <TextArea/>
            <Statistic
                total = {props.tasks.length}
                success = {getSuccessArrayLength(props.tasks)}
                pending = {props.tasks.length - getSuccessArrayLength(props.tasks)}
            />
            <Creation 
                onCreationOpen={props.onCreationOpen} 
                onChange={props.onCreationChange}
                inputTaskName={props.inputTaskName}
            />
            <TaskList
                tasks = {props.tasks}
                onDeleteTask={props.onDeleteTask}
                onCheckBoxClick = {props.onCheckBoxClick}
            />
            <DeleteAll onDeleteAll = {props.onDeleteAll}/>
        </main>
    )
} 

export default Content