import React from 'react'
import classes from './PriorutyDote.module.css'

const priorytyMap = {
    'Low Priority': classes.Blue,
    'Medium Priority': classes.Yellow,
    'High Priority': classes.Red,
}

const PriorutyDote = props => {
    const cls = [classes.PriorutyDote,  priorytyMap[props.type]]

    return(
        <div className = {cls.join(' ')}></div>
    )
}

export default PriorutyDote