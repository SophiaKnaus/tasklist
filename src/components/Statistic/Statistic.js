import React from 'react'
import classes from './Statistic.module.css'
import TextItem from '../TextItem/TextItem'



const Statistic = props => {
    return(
        <div className = {classes.Statistic}>
            <TextItem type="total" text={'Total'} value={props.total} />
            <TextItem type="success" text={'Succes'} value={props.success} />
            <TextItem type="pending" text={'Pending'} value={props.pending} />
        </div>
    )
}

export default Statistic