import React from 'react'
import classes from './TextItem.module.css'

const typeToClassMap = {
    total: classes.Total,
    success: classes.Success,
    pending: classes.Pending,
}

const TextItem = props => {
    const cls = [classes.TextItem, typeToClassMap[props.type]]

    return(
        <p className={cls.join(' ')}>{props.text}: {props.value}</p>
    )
} 

export default TextItem