import React, {Component} from 'react'
import classes from './Main.module.css'
import Content from '../Content/Content'
import CreateNewTask from "../CreateNewTask/CreateNewTask"

class Main extends Component {
    state = {
        tasks : [
            {
                name: 'Standup meeting with the team',
                priority: 'Low Priority',
                date: '10.07.2021',
                check: false,

            },

            {
                name: 'Order pizza for Granny tonoght',
                priority: 'High Priority',
                date: '10.07.2021',
                check: false,

            },

            {
                name: 'Design, Develop and Deploy Apps to Netlify for Client',
                priority: 'Medium Priority',
                date: '10.07.2021',
                check: false,

            },
        ],
        isCreationOpen: false,
        creationInputTaskName: '',
    }

    onDeleteTask = (taskToRemove) => {
        this.setState({
            tasks: this.state.tasks.filter(task => task !== taskToRemove)
        });
    }

    onDeleteAll = () => {
        this.setState ({
            tasks: []
        })
    }

    onCheckBoxClick = (task) => {
        task.check = !task.check;
        this.setState({
            tasks: [...this.state.tasks]
        })
    } 

    onCreationChange = (text) => {
        this.setState({
            creationInputTaskName : text
        })
    }

    onCreationOpen = () => {
        this.setState({
            isCreationOpen: true
        })
    }

    onCreationClose = () => {
        this.setState({
            isCreationOpen: false,
            creationInputTaskName: '',
        });
    }



    addCreatedTask = (task) => {
        this.setState({
            tasks: [...this.state.tasks, task],
        })
    }

    render (){
        return (
            <React.Fragment>        
                <section className = {classes.MainContent}>
                    <h3>trakk `em all!</h3>
                    <Content
                        tasks = {this.state.tasks}
                        onDeleteTask={this.onDeleteTask}
                        onDeleteAll = {this.onDeleteAll}
                        onCheckBoxClick = {this.onCheckBoxClick}
                        onCreationOpen={this.onCreationOpen}
                        onCreationChange={this.onCreationChange}
                        inputTaskName={this.state.creationInputTaskName}
                    />
                </section>
                <CreateNewTask 
                    isOpen={this.state.isCreationOpen} 
                    onClose={this.onCreationClose}
                    inputTaskName={this.state.creationInputTaskName}
                    addTask={this.addCreatedTask}
                />
            </React.Fragment>
            
        )
    }
}

export default Main