import React from 'react'
import classes from './Buttons.module.css'
import newImg from '../../pictures/create-new.svg'
import deleteImg from '../../pictures/delete-24px.svg'
import clear from '../../pictures/clear-24px.svg'
import done from '../../pictures/done-24px.svg'

const iconToImageMap = {
    new: newImg,
    delete: deleteImg,
    close: clear,
    ok: done,
}

const Buttons = props => {
    const imageSrc = props.icon
        ? iconToImageMap[props.icon]
        : newImg;

    return (
        <button className={classes.Buttons} onClick={props.onClick}>
            <img className={classes.Img} src={imageSrc} alt='button_img' />
        </button>
    )
}

export default Buttons