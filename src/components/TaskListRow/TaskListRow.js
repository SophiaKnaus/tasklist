import React from 'react'
import classes from './TaskListRow.module.css'

const TaskListRow = props => {
    return(
        <div className = {classes.TaskListRow}>
            {props.children}
        </div>
    )
}

export default TaskListRow